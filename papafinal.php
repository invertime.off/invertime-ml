<?php
$con = mysqli_connect("sql202.epizy.com", "epiz_25397860", "QdJ4i3To23VM", "epiz_25397860_idk");
if(!$con){
    echo 'Erreur SQL';
}
if(isset($_POST['submit'])){
    if(empty($_POST['numberint'])){
        header("Location: ./?error=numerononentré");
        exit();
    }
    else{
        $interventionNUM = $_POST['numberint'];
        if(empty($_POST['trajetaller1']) || empty($_POST['trajetaller2'])){
            header("Location: ./?error=trajetallernonentré");
            exit();
        }
        else{
            $trajetaller1 = $_POST['trajetaller1'];
            $trajetaller2 = $_POST['trajetaller2'];
            if(!empty($_POST['trajetretour1'])){
                $trajetretour1 = $_POST['trajetretour1'];
            }
            if(!empty($_POST['trajetretour2'])){
                $trajetretour2 = $_POST['trajetretour2'];
            }
            if(empty($_POST['kmdepart'])){
                header("Location: ./?error=kmdepartnonentré");
                exit();
            }
            else{
                $kmDepart = $_POST['kmdepart'];
                if(empty($_POST['kmarriver'])){
                    header("Location: ./?error=kmarrivénonentré");
                    exit();
                }
                else{
                    $kmArrive = $_POST['kmarriver'];
                    if(!empty($_POST['kmretour'])){
                        $kmRetour = $_POST['kmretour'];
                    }
                    if(empty($_POST['intervention1']) || empty($_POST['intervention2'])){
                        header("Location: ./?error=interventionnonentré");
                        exit();
                    }
                }
            }
        }
    }
    
}
?>
<!DOCTYPE HTML>
<html>
<head>
	<title>Intervention</title>
    <meta charset="utf-8" />
    <meta property="og:title" content="Intervention">
    <meta property="og:description" content="Invervention pour mon daron.">
    <meta property="og:image" content="https://invertime.ml/images/invertime_logo.png">
    <meta property="og:url" content="https://invertime.ml/">
    <meta property="og:site_name" content="Intervention">
    <meta name="twitter:image:alt" content="Intervention">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Invervention pour mon daron.">
    <meta name="author" content="Invertime & Vexcited">
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<link rel="stylesheet" href="./assets/css/main.css" />
	<link rel="icon" href="./images/invertime_logo.png"  />
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<style>
        input[type=date]{
            height: 35px;
            margin: 0 auto;
            width: 100%;
            font-family: arial, sans-serif;
            font-size: 18px;
            font-weight: bold;
            text-transform: uppercase;
            background-color: lighten(#2f2f2f,40%);
            outline: none;
            border: 0;
            border-radius: 3px;
            padding: 0 3px;
            color: #fff;
            }
        input[type=number] {
            -moz-appearance: textfield;
        }
        input::-webkit-inner-spin-button,
        input::-webkit-outer-spin-button { 
            -webkit-appearance: none;
            margin:0;
        }
        input::-o-inner-spin-button,
        input::-o-outer-spin-button { 
            -o-appearance: none;
            margin:0
        }
        input[type="number"] {
            text-align: center;
        }
        .label-file {
            cursor: pointer;
            color: #00b1ca;
            font-weight: bold;
        }
        .label-file:hover {
            color: #25a5c4;
        }
        .input-file {
            display: none;
        }
        #filedrag
        {
            display: none;
            font-weight: bold;
            text-align: center;
            padding: 1em 0;
            margin: 1em 0;
            color: #555;
            border: 2px dashed #555;
            border-radius: 7px;
            cursor: default;
        }

        #filedrag.hover
        {
            color: #f00;
            border-color: #f00;
            border-style: solid;
            box-shadow: inset 0 3px 4px #888;
        }
    </style>
</head>

<body>
	<header id="header" class="alt">
		<div class="logo">
            <a href="index">
                <img src="./images/invertime_logo.png" height="64">
            </a>
        </div>
			<a href="#menu">Menu</a>
	</header>
	<nav id="menu">
		<ul class="links">
			<li><a href="index">Home</a></li>
            <li><a href="websites">Websites</a></li>
			<li><a href="about">About</a></li>
		</ul>
	</nav>
<!-- Body Content -->

<section id="one" class="wrapper style3">
    <div class="inner">
        <header class="align-center">
            <p>Mail</p>
            <h2>Intervention</h2>
        </header>
    </div>
</section>

<section id="two" class="wrapper style2">
    <div class="inner">
        <div class="box">
            <div class="content">
                <header class="align-center">
                    <p>Intervention</p>
                </header>
                <div align="center">
                    <form method="post">
                        <label for="intervention">N°Intervention :</label>
                            <div class="6u 12u$(xsmall)">
                                <input type="number" name="numberint" id="intervention" value="0" placeholder="n°intervention" style="text-align:center" value="0000" required />
                            </div>
                        <label for="trajet">Trajets:</label>
                            <div class="6u 12u$(xsmall)">
                                <p>Aller</p>
                                    <input type="time" name="trajetaller1" id="trajet" value="" placeholder="trajet aller" required />
                                    <input type="time" name="trajetaller2" id="trajet" value="" placeholder="trajet aller" required />
                                <p>Retour</p>
                                    <input type="time" name="trajetretour1" id="trajet" value="" placeholder="trajet retour (Optionnel)" />
                                    <input type="time" name="trajetretour2" id="trajet" value="" placeholder="trajet retour (Optionnel)" />
                            </div>
                        <label for="km">Km:</label>
                            <div class="6u$ 12u$(xsmall)">
                                <input type="number" name="kmdepart" id="km" value="" placeholder="km depart" required />
                                <input type="number" name="kmarriver" id="km" value="" placeholder="km arrivé" required />
                                <input type="number" name="kmretour" id="km" value="" placeholder="km retour (optionnel)" />
                            </div>                     
                        <label for="intervention">Intervention:</label>
                            <div class="6u$ 12u$(xsmall)">
                                <input type="time" name="intervention1" id="intervention" value="" placeholder="intervention" required />
                                <input type="time" name="intervention2" id="intervention" value="" placeholder="intervention" required />
                                <br>
                            </div>
                        <label for="traveaux">Traveaux réalisés:</label>
                            <div class="12u$">
                                <textarea name="travauxréalisé" id="traveaux" placeholder="travaux" rows="6" required></textarea>
                            </div>
                            <div class="6u$ 12u$(xsmall)">
                                <label for="faitle">fait le :</label>
                                <input type="date" name="faitle" id="faitle" value=""/>
                            </div>
                            <div class="input-group mb-3">
                                <label for="faitle">fait par :</label>
                                <select class="select-test" style="width: auto;" id="nameselect" name="nameselect"> <!-- Ceci est le boutton -->
                                <?php
                                    $query = "SELECT * FROM employers";
                                    $result = mysqli_query($con, $query);
                                    if(mysqli_num_rows($result) > 0){
                                        while($row = mysqli_fetch_array($result)){
                                            $output .= '<option value="'.$row['name'].'">'.$row['name'].'</option>';
                                        }
                                        echo $output;
                                    }
                                ?>
                                    <option value="NULL" selected></option>
                                </select>
                                <input type="text" name="nameinput" id="nameinput" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="fileselect">Files to upload:</label>
                                <input type="file" id="fileselect" name="fileselect[]" multiple="multiple" accept="image/*" value=""/>
                                <div id="filedrag">or drop files here</div>
                                <div id="messages">
                                <p>informarions sur l'image(ou les images)</p>
                                </div>
                            </div>
                            <div class="12u$">
                                <ul class="actions">
                                    <li><input type="submit" name="submit" value="envoyer le mail" /></li>
                                    <li><input type="reset" value="Reset" class="alt" /></li>
                                </ul>
                            </div>
                        </form>
                    </div>
                </div>
			</div>
        </div>
</section>

<!-- End Content -->
<footer id="footer">
	<div class="container">
		<ul class="icons">
			<li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
			<li><a href="https://allmy.games/user/invertime.off9830/collection" class="icon fa-gamepad"><span class="label">all my game</span></a></li>
			<li><a href="#" class="icon fa-instagram"><span class="label">Instagram</span></a></li>
			<li><a href="mailto:invertime.off@gmail.com" class="icon fa-envelope-o"><span class="label">Email</span></a></li>
		</ul>
	</div>
	<div class="copyright">&copy; invertime All rights reserved.</div>
</footer>
</body>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <script src="./assets/js/skel.min.js"></script>
    <script src="./assets/js/util.js"></script>
    <script src="./assets/js/main.js"></script>
    <script>
        function validateAndUpload(input){
            var URL = window.URL || window.webkitURL;
            var file = input.files[0];
            var fileInput = document.getElementById('fileselect');   
            var filename = fileInput.files[0].name;

            if (file) {
                var image = new Image();

                image.onload = function() {
                    if (this.width) {
                        alert(filename + ' a bien été upload');
                        //TODO: upload to backend
                    }
                };

                image.src = URL.createObjectURL(file);
            }
        };
    </script>
<script>
function $id(id) {
	return document.getElementById(id);
}

function Output(msg) {
	var m = $id("messages");
	m.innerHTML = msg + m.innerHTML;
}
if (window.File && window.FileList && window.FileReader) {
	Init();
}

function Init() {

	var fileselect = $id("fileselect"),
		filedrag = $id("filedrag"),
		submitbutton = $id("submitbutton");

	fileselect.addEventListener("change", FileSelectHandler, false);

	var xhr = new XMLHttpRequest();
	if (xhr.upload) {
	
		filedrag.addEventListener("dragover", FileDragHover, false);
		filedrag.addEventListener("dragleave", FileDragHover, false);
		filedrag.addEventListener("drop", FileSelectHandler, false);
		filedrag.style.display = "block";
		
		submitbutton.style.display = "none";
	}

}
function FileDragHover(e) {
	e.stopPropagation();
	e.preventDefault();
	e.target.className = (e.type == "dragover" ? "hover" : "");
}
function FileSelectHandler(e) {

FileDragHover(e);

var files = e.target.files || e.dataTransfer.files;

for (var i = 0, f; f = files[i]; i++) {
    ParseFile(f);
}

}

function ParseFile(file) {

Output(
    "<p>File information: <strong>" + file.name +
    "</strong> type: <strong>" + file.type +
    "</strong> size: <strong>" + file.size +
    "</strong> bytes</p>"
);

}

</script>
<script>
    var e = document.getElementById("nameselect");
     e.addEventListener("change", function() {

    var val = e.options[e.selectedIndex].text;
    document.getElementById("nameinput").value = val;

    });

    var val = e.options[e.selectedIndex].text;
    document.getElementById("nameinput").v
</script>
<script>
function resetFile() { 
            const file = 
            document.getElementById('fileselect'); 
            file.value = ''; 
        } 
        window.onbeforeunload = resetFile;
</script>
</html>