<!DOCTYPE HTML>

<html>
	<head>
		<title>invertime</title>
        <meta charset="utf-8" />
        <meta property="og:title" content="home">
        <meta property="og:description" content="website of invertime ^^">
        <meta property="og:image" content="http://invertime.ml/images/invertime_logo.png">
        <meta property="og:url" content="http://invertime.ml/">
        <meta property="og:site_name" content="invertime">
        <meta name="twitter:image:alt" content="invertime website">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="website of invertime">
        <meta name="author" content="Invertime">
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<link rel="stylesheet" href="assets/css/main.css" />
		<link rel="icon" href="./images/invertime_logo.png"  />
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
		<style>
                                            input[type=date] {
                                                    height: 35px;
                                                    margin: 0 auto;
                                                    width: 100%;
                                                    font-family: arial, sans-serif;
                                                    font-size: 18px;
                                                    font-weight: bold;
                                                    text-transform: uppercase;
                                                    background-color: lighten(#2f2f2f,40%);
                                                    outline: none;
                                                    border: 0;
                                                    border-radius: 3px;
                                                    padding: 0 3px;
                                                    color: #fff;}
                                            </style>
	</head>
    <body>
		<!-- Header -->
			<header id="header" class="alt">
			<div class="logo" ><a href="index"> <img src="./images/invertime_logo.png" height="64"> </a></div>
				<a href="#menu">Menu</a>
			</header>

		<!-- Nav -->
			<nav id="menu">
				<ul class="links">
					    <li><a href="index">Home</a></li>
					    <li><a href="websites">Websites</a></li>
                        <li><a href="about">About</a></li>
				</ul>
			</nav>



