<?php $indexpage=1; $aboutpage=0; include('header.php'); ?>


		<!-- Banner -->
				<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
  </ol>
<div class="carousel-inner">
    <div class="carousel-item active">
		<img class="d-block w-100" src="images/back_1.jpg" alt="">
		<div class="carousel-caption d-none d-md-block">
			<p>watch my youtube channel : <a href="https://www.youtube.com/channel/UCSkt7cMtpSI_igBh78zUv4Q?view_as=subscriber" class="text-light">invertime</a></p>
			<h2>(ﾉ◕ヮ◕)ﾉ*:･ﾟ✧</h2>
		</div>
	</div>
    <div class="carousel-item">
	  	<img class="d-block w-100" src="images/back_2.jpg" alt="">
		<div class="carousel-caption d-none d-md-block">
			<p>listen to my music : <a href="https://songwhip.com/song/summer/summer" class="text-light">invertime</a></p>
			<h2>(〜￣▽￣)〜</h2>
		</div>
    </div>
</div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>

		<!-- One -->
			<section id="one" class="wrapper style2">
				<div class="inner">
					<div class="grid-style">

						<div>
							<div class="box">
								<div class="image fit">
									<img src="images/back_1.jpg" alt="" />
								</div>
								<div class="content">
									<header class="align-center">
										<p>my youtube channel</p>
										<h2>Invertime</h2>
									</header>
									<p align=center> On my youtube channel I make: ovewatch, music, minecraft, and some cool things! Enjoy ^^ </p>
									<footer class="align-center">
										<a href="https://www.youtube.com/channel/UCSkt7cMtpSI_igBh78zUv4Q?view_as=subscriber" class="button alt">Youtube Channel</a>
									</footer>
								</div>
							</div>
						</div>

						<div>
							<div class="box">
								<div class="image fit">
									<img src="images/back_2.jpg" alt="" />
								</div>
								<div class="content">
									<header class="align-center">
										<p>my musics</p>
										<h2>Invertime</h2>
									</header>
									<div align=center>
									<p> I make some electro like futur bass and electro chill! Enjoy ^^ </p>
									</div>
									<br>
									<footer class="align-center">
										<a href="https://soundcloud.com/invert-time" class="button alt">My Musics</a>
									</footer>
								</div>
							</div>
						</div>

					</div>
				</div>
			</section>

		<!-- Two -->
			<section id="two" class="wrapper style3">
				<div class="inner">
					<header class="align-center">
						<p>Nam vel ante sit amet libero scelerisque facilisis eleifend vitae urna</p>
						<h2>Morbi maximus justo</h2>
					</header>
				</div>
			</section>



		<!-- Footer -->
		<?php include('footer.php'); ?>			